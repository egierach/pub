from django.core.management.base import BaseCommand, CommandError
from enc.operations import encrypt_secrets, secrets_are_encrypted
import getpass

class Command(BaseCommand):
    help = 'Encrypts project secrets file'

    def handle(self, *args, **options):
        if not secrets_are_encrypted():
            password = getpass.getpass('Enter password: ')
            encrypt_secrets(password)
            if secrets_are_encrypted():
                self.stdout.write('Successfully encrypted secrets')
        else:
            self.stdout.write('Secrets are already encrypted')

