from django.core.management.base import BaseCommand, CommandError
from enc.operations import reencrypt_secrets, encrypt_secrets, secrets_are_encrypted
import getpass

class Command(BaseCommand):
    help = 'Allows you to change the password that encrypts the project secrets file'

    def handle(self, *args, **options):
        if secrets_are_encrypted():
            old_password = getpass.getpass('Enter old password: ')
            new_password = getpass.getpass('Enter new password: ')
            new_confirm = getpass.getpass('Confirm new password: ')
            if new_password and new_password == new_confirm:
                reencrypt_secrets(new_password, old_password)
            else:
                raise Exception('Your passwords were blank or did not match')
        else:
            password = getpass.getpass('Enter password: ')
            confirm = getpass.getpass('Confirm password: ')
            if password and password == confirm:
                encrypt_secrets(password)
            else:
                raise Exception('Your passwords were blank or did not match')
        if secrets_are_encrypted():
            self.stdout.write('Successfully encrypted secrets')

