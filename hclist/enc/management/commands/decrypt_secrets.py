from django.core.management.base import BaseCommand, CommandError
from enc.operations import decrypt_secrets, secrets_are_encrypted
import getpass

class Command(BaseCommand):
    help = 'Decrypts project secrets file'

    def handle(self, *args, **options):
        if secrets_are_encrypted():
            password = getpass.getpass('Enter password: ')
            decrypt_secrets(password)
            if not secrets_are_encrypted():
                self.stdout.write('Successfully decrypted secrets')
        else:
            self.stdout.write('Secrets are not encrypted')

