"""
This module contains functions to encrypt/decrypt the project secrets file.

Public Functions:
secrets_are_encrypted -- returns True if the secrets file is encrypted
decrypt_secrets -- decrypts the secrets file using the passed-in password
encrypt_secrets -- encrypts the secrets file using the passed-in password
reencrypt_secrets -- decrypts and re-encrypts the secrets file using a different password

Module Constants:
BLOCK_SIZE -- the AES block size, used for the IV and padding the cleartext message
PAD_CHAR -- the character used to pad the cleartext message to a multiple of BLOCK_SIZE

Encryption is CBC mode AES with randomized initialization vector prepended to file.
"""
from django.conf import settings
from Crypto import Random
from Crypto.Cipher import AES
from Crypto.Hash import SHA256

BLOCK_SIZE = 16
PAD_CHAR = '|'

class EncOpError(Exception):
    """Exception class used by encryption functions in this module"""
    pass

def _is_cleartext(string):
    return True and 'CLEARTEXT' in string

def secrets_are_encrypted():
    """Returns False if the first line of the file contains the text 'CLEARTEXT'"""
    result = False
    with open(settings.SECRETS_FILE, 'r') as sf:
        result = not _is_cleartext(sf.readline())
    return result

def _get_cipher(password, iv=''):
    """Helper function to instantiate a Crypto.Cipher with the correct settings"""
    hash_obj = SHA256.new()
    hash_obj.update(password)
    key = hash_obj.digest()
    if not iv:
        rnd_obj = Random.new()
        iv = rnd_obj.read(BLOCK_SIZE)
    return (AES.new(key, AES.MODE_CBC, iv), iv)

def decrypt_secrets(password):
    """
    Decrypts the secrets file using the passed-in password.
    
    Writes the decrypted text back to the same file.
    """
    if secrets_are_encrypted():
        with open(settings.SECRETS_FILE, 'r') as sf:
            encrypted_text = sf.read()
            iv = encrypted_text[:BLOCK_SIZE]
            encrypted_text = encrypted_text[BLOCK_SIZE:]
            (cipher, iv) = _get_cipher(password, iv)
            plain_text = cipher.decrypt(encrypted_text).rstrip(PAD_CHAR)
            if _is_cleartext(plain_text):
                with open(settings.SECRETS_FILE, 'w') as sfw:
                    sfw.write(plain_text)
            else:
                raise EncOpError("Incorrect password supplied for secrets file")

def encrypt_secrets(password):
    """
    Encrypts the secrets file using the passed-in password.

    Writes the encrypted bytestring back to the same file.
    """
    if not secrets_are_encrypted():
        if not password:
            raise EncOpError("You did not supply a password with which to encrypt the secrets file.")
        with open(settings.SECRETS_FILE, 'r') as sf:
            plain_text = sf.read()
            plain_text += (BLOCK_SIZE - (len(plain_text) % BLOCK_SIZE)) * PAD_CHAR
            (cipher, iv) = _get_cipher(password)
            encrypted_text = iv + cipher.encrypt(plain_text)
            with open(settings.SECRETS_FILE, 'w') as sfw:
                sfw.write(encrypted_text)

def reencrypt_secrets(password, old_password):
    """Convenience method to decrypt and re-encrypt the secrets file.  Useful when changing passwords."""
    decrypt_secrets(old_password)
    encrypt_secrets(password)

