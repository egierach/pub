"""
This module defines the VM base and VMError exception classes.

The VM base class represents a virtual linux machine on which
to perform various operations or host services.  The VM base
class contains methods to perform tasks commonly needed when
setting up or refreshing a machine created from an image.

Role-specific setup should be handled by consuming apps (for
example, setting up nginx or installing vhost configs).

Provider-specific setup should be handled by VM subclasses.

This module assumes that an ssh connection to the remote host
as the current OS user will work.  Instantiating a VM from
one of the Provider classes should take care of this for you.
"""

from django.conf import settings
from fabric.api import local, get, put, run, settings as fabset
from contextlib import contextmanager
import os

class VM(object):
    """
    Base class representing a virtual linux machine.

    Instance Vars:
    name -- the name of this VM
    host -- the SSH host address

    Public Methods:
    export_db -- copy VM db to local db
    import_db -- copy local db to VM db
    import_local_archive -- copy arbitrary .tar.gz file to VM
    import_repo_rev -- copy specific revision of git repo to VM
    import_repo_wc -- copy local working copy of git repo to VM
    update_virtualenv -- update VM virtualenv to match local
    """
    name = ''
    host = 'localhost'
    DB_PASS_FILE = '~/.pgpass'
    DB_TMP_FILE = '~/db.custom'
    REPO_TMP_FILE = '~/repo.tar.gz'
    PIP_REQ_TMP_FILE = '~/pip.reqs'

    def __init__(self, **kwargs):
        """
        Sets variables for VM instances.

        Available Keyword Args:
        name -- the name of this VM instance
        host -- the SSH host address of the real-world VM represented by this instance.

        Note: the 'host' instance variable must be set before calling most
        of the instance methods provided by this class.
        """
        self.name = kwargs.get('name', self.name)
        self.host = kwargs.get('host', self.host)

    def export_db(self, db_profile_name, tables=[], schemas=[]):
        """
        Copies database content and definition from remote VM host to localhost.

        Args:
        db_profile_name -- key value for django settings.DATABASES dict
        tables -- optional list of tables to copy.  if omitted, all tables
            are copied.
        schemas -- optional list of schemas to copy.  if omitted, all schemas
            are copied.

        Postgresql is assumed as this method uses pg_dump and pg_restore.
        """
        # creates .pgpass file so auth is transparent
        self._make_pg_pass_files(db_profile_name)
        with self._envset():
            db_name = settings.DATABASES[db_profile_name]['NAME']
            db_user = settings.DATABASES[db_profile_name]['USER']
            dump_result = run(self._get_dump_command(db_user, db_name, tables, schemas))
            if dump_result.failed:
                raise VMError('Failed to create pg_dump file on remote vm host.  Command: {}'.format(dump_result.real_command))
            get_result = get(self.DB_TMP_FILE, self.DB_TMP_FILE)
            if get_result.failed:
                raise VMError('Failed to download remote pg_dump file from remote vm host: {}'.format(self.host))
            restore_result = local(self._get_restore_command(db_user, db_name))
            if restore_result.failed:
                raise VMError('Failed to restore pg_dump file into local database.  Command: {}'.format(restore_result.real_command))

    def import_db(self, db_profile_name, tables=[], schemas=[]):
        """
        Copies database content and definition from localhost to remote VM host.

        Args:
        db_profile_name -- key value for django settings.DATABASES dict
        tables -- optional list of tables to copy.  if omitted, all tables
            are copied.
        schemas -- optional list of schemas to copy.  if omitted, all schemas
            are copied.

        Postgresql is assumed as this method uses pg_dump and pg_restore.
        """
        self._make_pgpass_files(db_profile_name)
        with self._envset():
            db_name = settings.DATABASES[db_profile_name]['NAME']
            db_user = settings.DATABASES[db_profile_name]['USER']
            dump_result = local(self._get_dump_command(db_user, db_name, tables, schemas))
            if dump_result.failed:
                raise VMError('Failed to create local pg_dump file.  Command: {}'.format(dump_result.real_command))
            put_result = put(self.DB_TMP_FILE, self.DB_TMP_FILE)
            if put_result.failed:
                raise VMError('Failed to upload local pg_dump file to remote vm host: {}'.format(self.host))
            restore_result = run(self._get_restore_command(db_user, db_name))
            if restore_result.failed:
                raise VMError('Failed to restore pg_dump file into remote database.  Command: {}'.format(restore_result.real_command))

    def import_local_archive(self, archive_path, dest_dir):
        """
        Copies a local .tar.gz file to the remote VM host and extracts it into desired directory.

        Args:
        archive_path -- filesystem path to local archive file
        dest_dir -- filesystem path on remote VM host to extract archive file
        """
        with self._envset():
            put_result = put(local_path=archive_path, remote_path=archive_path)
            if put_result.failed:
                raise VMError('Failed to upload archive to remote vm host: {}'.format(self.host))
            mkdir_result = run('mkdir -p {}'.format(dest_dir))
            if mkdir_result.failed:
                raise VMError('Failed to create directory {} on remote vm host'.format(dest_dir))
            extract_result = run('tar -xzf {} -C {}'.format(archive_path, dest_dir))
            if extract_result.failed:
                raise VMError('Failed to extract archive to {} on remote vm host'.format(dest_dir))

    def import_repo_rev(self, dest_dir, revision='HEAD'):
        """
        Archives the current local git repo at a specified revision and extracts it on the remote VM host.

        Args:
        dest_dir -- filesystem path on remote VM host to extract archive file
        revision -- optional git revision or branch.  default is HEAD
        """
        with self._envset():
            archive_result = local('git archive --format=tar.gz {} > {}'.format(revision, self.REPO_TMP_FILE))
            if archive_result.failed:
                raise VMError('Failed to archive local git revision.  Command: {}'.format(archive_result.real_command))
        self.import_local_archive(self.REPO_TMP_FILE, dest_dir)

    def import_repo_wc(self, dest_dir):
        """
        Archives the local working copy of the current git repo and extracts it on the remote VM host.

        Args:
        dest_dir -- filesystem path on remote VM host to extract archive file
        """
        with self._envset():
            root_result = local('git rev-parse --show-toplevel', capture=True)
            if root_result.failed:
                raise VMError('Failed to obtain local git repo root folder.  Command: {}'.format(root_result.real_command))
            archive_result = local('tar -czf {} --exclude .git --exclude venv -C {} .'.format(self.REPO_TMP_FILE, root_result.strip()))
            if archive_result.failed:
                raise VMError('Failed to archive local git repo.  Command: {}'.format(archive_result.real_command))
        self.import_local_archive(self.REPO_TMP_FILE, dest_dir)

    def update_virtualenv(self):
        """
        Creates or updates a python virtualenv on the remote VM host to match the local one.

        TODO: this
        """
        pass

    def _get_dump_command(self, db_user, db_name, tables, schemas):
        """
        Formulates a pg_dump cmdline invocation that can be executed
        either with subprocess calls or fabric api functions.

        Args:
        db_user -- the username to connect to Postgresql as
        db_name -- the name of the database to restore ddl/dml into
        tables -- a list of tables to include in the dump.  If empty,
            all tables are included.
        schemas -- a list of schema names to include in the dump.
            If empty, all schemas are included.

        Returns a string containing the command and its arguments

        This is the counterpart to _get_restore_command.  Dumps it
        produces should be able to be restored by that method.
        """
        return 'pg_dump -w -U {} {} -Fc {} {} > {}'.format(
            db_user,
            ' '.join(["-t '{}'".format(x) for x in tables]),
            ' '.join(["-n '{}'".format(x) for x in schemas]),
            db_name,
            self.DB_TMP_FILE
        )

    def _get_restore_command(self, db_user, db_name):
        """
        Formulates a pg_restore cmdline invocation that can be executed
        either with subprocess calls or fabric api functions.

        Args:
        db_user -- the username to connect to Postgresql as
        db_name -- the name of the database to restore ddl/dml into

        Returns a string containing the command and its arguments

        This is the counterpart to _get_dump_command and should be able
        to restore dumps created by that method.
        """
        return 'pg_restore -w -U {} -d {} -c --no-data-for-failed-tables {}'.format(
            db_user,
            db_name,
            self.DB_TMP_FILE
        )

    def _make_pgpass_files(self, db_profile_name):
        """
        Creates .pgpass files locally and on the remote VM host for promptless Postgresql auth.

        Args:
        db_profile_name -- key value for django settings.DATABASES dict
        """
        with self._envset():
            db_name = settings.DATABASES[db_profile_name]['NAME']
            db_user = settings.DATABASES[db_profile_name]['USER']
            db_pass = settings.DATABASES[db_profile_name]['PASSWORD']
            db_host = settings.DATABASES[db_profile_name].get('HOST', '')
            db_port = settings.DATABASES[db_profile_name].get('PORT', '')
            with open(os.path.expanduser(self.DB_PASS_FILE), 'w') as pf:
                pf.write('{}:{}:{}:{}:{}\n'.format(
                    db_host if db_host else '*',
                    db_port if db_port else '*',
                    db_name,
                    db_user,
                    db_pass
                ))
            chmod_result = local('chmod 0600 {}'.format(self.DB_PASS_FILE))
            if chmod_result.failed:
                raise VMError('Failed to chmod local .pgpass file.  Command: {}'.format(chmod_result.real_command))
            put_result = put(self.DB_PASS_FILE, self.DB_PASS_FILE)
            if put_result.failed:
                raise VMError('Failed to upload .pgpass file to remote vm host: {}'.format(self.host))
            chmod_result = run('chmod 0600 {}'.format(self.DB_PASS_FILE))
            if chmod_result.failed:
                raise VMError('Failed to chmod remote .pgpass file.  Command: {}'.format(chmod_result.real_command))

    @contextmanager
    def _envset(self):
        """Convenience wrapper for fabric environment settings context manager"""
        with fabset(host_string=self.host, key_filename='~/.ssh/id_rsa'):
            yield

class VMError(Exception):
    """Exception class for errors raised by the VM class and its subclasses"""
    pass

