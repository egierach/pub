"""
This module defines the Droplet VM subclass.
"""
from cloud.vms.vm import VM

class Droplet(VM):
    """
    This class defines a DigitalOcean-specific subclass
    of the VM base class.

    Instance Vars:
    id -- the DigitalOcean-provided ID of the real-world droplet
    represented by this instance.

    The real reason for creating this subclass is to allow
    instance variables to be parsed from the output of
    DigitalOcean droplet inquiries.
    """
    id = ''
    
    def __init__(self, config={}):
        self.id = config.get('id', '')
        self.name = config.get('name', '')
        try:
            for interface in config['networks']['v4']:
                if interface['type'] == 'public':
                    self.host = interface['ip_address']
                    break
        except KeyError:
            pass
        super(VM, self).__init__()

