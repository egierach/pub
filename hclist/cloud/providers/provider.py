"""
This module defines the Provider base and ProviderError
exception classes.

The Provider base class and its subclasses implement
APIs that provision and destroy virtual machines
on various cloud hosting platforms.
""" 
from django.conf import settings
from subprocess import check_call, check_output
import os

class Provider(object):
    """
    Base class defining a common interface for cloud hosting provider APIs.

    Instance Methods:
    create_vm -- provision a new virtual machine
    destroy_vm -- destroy/deprovision an existing virtual machine
    get_or_create_vm -- returns an existing or newly-provisioned VM if none exists
    get_or_create_local_ssh_key -- returns an existing or newly-generated SSH key

    Class Methods:
    factory -- creates and returns a new Provider or Provider subclass instance
        based on django settings
    
    Class Constants:
    LOCAL_KEY_FILE -- path to SSH key to be used for SSH auth to VMs
    """
    LOCAL_KEY_FILE = '~/.ssh/id_rsa'

    def create_vm(self, name):
        """Provisions a new virtual machine with a hosting provider and returns a VM instance"""
        raise NotImplementedError('Provider subclass did not implement create_vm method')

    def destroy_vm(self, vm):
        """Deprovisions an existing virtual machine"""
        raise NotImplementedError('Provider subclass did not implement destroy_vm method')

    def get_or_create_vm(self, name):
        """Returns a VM instance referring to an existing or newly-created virtual machine"""
        raise NotImplementedError('Provider subclass did not implement get_or_create_vm method')

    def get_or_create_local_ssh_key(self):
        """
        Finds or generates an SSH key that can be used for authenticating to VMs.

        Returns a tuple containing the following:
            - ssh key name (user@host)
            - ssh key fingerprint
            - the full content of the public key as a bytestring
        """
        key_file = os.path.expanduser(self.LOCAL_KEY_FILE)
        if not os.path.exists(key_file):
            check_call('ssh-keygen -t rsa -q -N "" -f {}'.format(key_file), shell=True)
        output = check_output('ssh-keygen -lf {}'.format(key_file), shell=True)
        key_info = output.split()
        key_text = ''
        with open('{}.pub'.format(key_file), 'r') as kf:
            key_text = kf.read()
        #return ('user@host', '0f:d3:45:...', '9g0n243<g#sdg^23....')
        return (key_info[2], key_info[1], key_text)

    @classmethod
    def factory(cls, profile_name):
        """
        Instantiates and returns a Provider or Provider subclass based on django settings.

        Args:
        profile_name -- key value in the settings.CLOUD_PROFILES dict

        settings.CLOUD_PROFILES[profile_name] is itself a dict with settings that
        are largely provider-specific.  The only required key/value pair in this dict
        is 'provider_class': 'path.to.provider.subclass'.
        """
        if profile_name in settings.CLOUD_PROFILES:
            mod_name_pieces = settings.CLOUD_PROFILES[profile_name]['provider_class'].rsplit('.', 1)
            provider_module = __import__(mod_name_pieces[0], fromlist=[mod_name_pieces[1]])
            return getattr(provider_module, mod_name_pieces[1])(profile_name, settings.CLOUD_PROFILES[profile_name])
        return cls()

class ProviderError(Exception):
    """Exception class for errors raised by the Provider class and its subclasses"""
    pass

