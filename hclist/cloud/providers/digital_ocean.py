"""
This module defines the DigitalOcean Provider subclass which is used to
integrate with DigitalOcean's API services.
"""
from cloud.providers.provider import Provider, ProviderError
from cloud.vms.droplet import Droplet
from datetime import datetime, timedelta
import requests
import json
import re
import time

class DigitalOcean(Provider):
    """
    This class facilitates the creation, setup and destruction
    of DigitalOcean VMs, also known as Droplets.

    DigitalOcean is a virtual machine hosting provider, similar
    to Linode or Slicehost.  It provides a RESTful API to create,
    delete and inquire about droplets, disk images, SSH keys
    and other facets of their service.

    Instance Variables:
    profile_name -- key value in the settings.CLOUD_PROFILES dict
    oauth_token -- cryptographic token identifying you, the customer
    image_id -- identifies a specific disk image or system snapshot
        that can be loaded onto a droplet.
    droplet_size -- a slug identifying which size of virtual machine
        to provision

    Instance Methods:
    create_vm -- provisions a new droplet
    destroy_vm -- deprovisions the droplet represented by the passed-in VM instance
    destroy_vm_by_name -- same as destroy_vm, but instantiates a VM by name first
    get_or_create_ssh_key -- registers a new SSH key with DigitalOcean and returns its ID
    get_or_create_vm -- either finds or provisions a named droplet and returns it
    instantiate_vm -- retrieves an existing droplet and returns a VM representing it

    Class Variables:
    API_URL -- base URL for all DigitalOcean API calls
    CREATION_WAIT_TIMEOUT -- timedelta object representing the maximum amount of time
        to wait before giving up on a create_vm request.  Some very large disk images can
        take a really long time to load.
    CREATION_WAIT_POLL_INTERVAL -- number of seconds to wait before checking on the
        status of a pending create_vm request.
    """
    profile_name = ''
    oauth_token = ''
    image_id = ''
    droplet_size = ''
    API_URL = 'https://api.digitalocean.com/v2'
    CREATION_WAIT_TIMEOUT = timedelta(hours=4)
    CREATION_WAIT_POLL_INTERVAL = 20
    
    def __init__(self, profile_name='', profile={}):
        """Harvests values for instance variables from Django settings"""
        self.profile_name = profile_name
        self.oauth_token = profile.get('oauth_token', '')
        self.image_id = profile.get('image_id', '')
        self.droplet_size = profile.get('droplet_size', '')

    def create_vm(self, name):
        """
        Creates a droplet and returns a Droplet instance.

        Args:
        name -- name of the droplet to be created.  This must be unique for
            your account and will show up in the DigitalOcean web UI in your
            droplet list after creation has completed.

        This method auto-selects a data center (region) within which to
        create your droplet based on availability of your disk image and
        your requested droplet size.

        This method waits until provisioning is complete and the droplet has
        been booted (see self._wait_for_status).
        The initial creation POST request returns immediately, but data like
        the host address aren't available until after it's been powered on.
        """
        (image_id, available_regions) = self._get_image()
        region_slug = self._get_region(available_regions)
        (keyname, fingerprint, keytext) = self.get_or_create_local_ssh_key()
        key_id = self.get_or_create_ssh_key(keyname, fingerprint, keytext)
        request_body = {
            'name': name,
            'image': image_id,
            'region': region_slug,
            'size': self.droplet_size,
            'ssh_keys': [key_id,],
        }
        droplet_details = self._do_request('/droplets', 'post', request_body)
        if 'droplet' in droplet_details:
            self._wait_for_status(droplet_details['droplet']['id'], 'active', ['new',])
            return self.instantiate_vm(name)
        raise ProviderError('Could not create DigitalOcean virtual machine')

    def destroy_vm(self, vm):
        """
        Destroys the droplet represented by the passed in Droplet instance.

        Args:
        vm -- Droplet instance with ID variable populated

        This method returns immediately after the request has been accepted
        by DigitalOcean.  DigitalOcean asynchronously deletes the droplet
        afterward.

        TODO: test whether this can be immediately followed up by a create_vm
        request with the same name.
        """
        self._do_request('/droplets/{}'.format(vm.id), 'delete', '')

    def destroy_vm_by_name(self, name):
        """
        Destroys the droplet identified by the passed in name.

        Args:
        name -- the name of an existing droplet.

        This method mainly exists as a shortcut.
        """
        self.destroy_vm(self.instantiate_vm(name))

    def get_or_create_ssh_key(self, keyname, fingerprint, keytext):
        """
        Registers an SSH key with DigitalOcean for use in create_vm requests.
        Returns a DigitalOcean-supplied ID.

        Args:
        keyname -- display name of key in DigitalOcean web UI
        fingerprint -- identifier of key for DigitalOcean API requests
        keytext -- the public key file contents, as a bytestring
        """
        try:
            key_details = self._do_request('/account/keys/{}'.format(fingerprint), 'get', '')
        except requests.exceptions.HTTPError:
            request_body = {
                'name': keyname,
                'public_key': keytext,
            }
            key_details = self._do_request('/account/keys', 'post', request_body)
        if 'ssh_key' in key_details:
            return key_details['ssh_key']['id']
        raise ProviderError('Could neither find nor create SSH key for {} on DigitalOcean'.format(keyname)) 

    def get_or_create_vm(self, name):
        """
        Retrieves or creates a VM by name.

        Wrapper for instantiate_vm and create_vm.
        """
        try:
            return self.instantiate_vm(name)
        except ProviderError:
            return self.create_vm(name)

    def instantiate_vm(self, name):
        """
        Creates and returns a Droplet instance representing an existing
        DigitalOcean droplet.

        Args:
        name -- the name identifying which droplet to get
        """
        droplets = self._do_request('/droplets', 'get', '')
        if 'droplets' in droplets:
            for droplet in droplets['droplets']:
                if droplet['name'] == name:
                    return Droplet(droplet)
        raise ProviderError('Could not find droplet with name={}'.format(name))

    def _do_request(self, path, method, body):
        """Wrapper for DigitalOcean API requests; returns JSON-decoded response body."""
        headers = {
            'Authorization': 'Bearer {}'.format(self.oauth_token),
            'Content-Type': 'application/json',
        }
        response = getattr(requests, method)(self.API_URL + path, data=json.dumps(body), headers=headers)
        response.raise_for_status()
        if response.text:
            return response.json()

    def _get_image(self):
        """Retrieves details about a particular disk image from DigitalOcean"""
        image_details = self._do_request('/images/{}'.format(self.image_id), 'get', '')
        return (image_details['image']['id'], image_details['image']['regions'])

    def _get_region(self, from_slugs=[]):
        """
        Returns a slug identifying a valid region (data center) within which to create a droplet.

        Args:
        from_slugs -- optional list of region slugs to choose from

        Regions returned by this method must be marked "available" by DigitalOcean,
        contain the desired droplet size and (if defined) be one of the choices
        in 'from_slugs'.
        """
        available_regions = self._do_request('/regions', 'get', '')
        for region in available_regions['regions']:
            if region['available'] and self.droplet_size in region['sizes'] and (not from_slugs or region['slug'] in from_slugs):
                return region['slug']
        raise ProviderError('No DigitalOcean regions found matching our criteria')

    def _wait_for_status(self, id, goal_status, continue_statuses):
        """
        Waits for a droplet to transition to a specified status.

        Args:
        id -- DigitalOcean-provided ID of the droplet in question
        goal_status -- the droplet status signifying successful completion of operation
        continue_statuses -- a list of statuses that are considered valid intermediate
            states on the road to "goal_status".

        One use case:  when droplets are created, the API sends a successful response
        immediately and then queues an asynchronous job to actually perform the fulfillment,
        which involves provisioning the droplet, loading a disk image, customizing settings
        and booting the machine.  The initial status of a droplet is "new".  Once all of this
        work has completed and the machine is ready for use, the status is "active".
        """
        give_up_time = datetime.now() + self.CREATION_WAIT_TIMEOUT
        while datetime.now() < give_up_time:
            droplet_details = self._do_request('/droplets/{}'.format(id), 'get', '')
            if 'droplet' in droplet_details:
                if droplet_details['droplet']['status'] == goal_status:
                    return
                elif droplet_details['droplet']['status'] not in continue_statuses:
                    raise ProviderError('DigitalOcean droplet has an unexpected status: {}'.format(droplet_details['droplet']['status']))
                time.sleep(self.CREATION_WAIT_POLL_INTERVAL)
                continue
            raise ProviderError('Could not find droplet details on DigitalOcean for id={}'.format(id))

