from django.core.management.base import BaseCommand, CommandError
from cloud.providers.provider import Provider

class Command(BaseCommand):
    help = 'Tests VM creation, installation and deletion'

    def handle(self, *args, **options):
        dig_ocean = Provider.factory('test_ocean')
        self.stdout.write('Provider successfully instantiated')
        vm = dig_ocean.get_or_create_vm('testvm')
        self.stdout.write('VM successfully retrieved or created')
        vm.import_repo_wc('/opt/testvm')
        self.stdout.write('Working directory successfully uploaded')
        vm.import_repo_rev('/opt/testvm2')
        self.stdout.write('HEAD revision successfully uploaded')
        vm.update_virtualenv()
        self.stdout.write('Virtualenv successfully updated')
        vm.import_db('default', tables=['django_site',])
        self.stdout.write('Database successfully uploaded')
        #dig_ocean.destroy_vm(vm)
        #self.stdout.write('VM successfully destroyed')
        self.stdout.write('All done')

