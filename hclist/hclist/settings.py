import os

"""
Most django projects have some sort of "local settings" or other
construct that allows them to store their sensitive settings
without promoting them to the VCS in plaintext.

The secrets.py file has these settings.  A pre-commit hook
encrypts it before adding it to staging area.  See the "enc"
app for details.
"""
try:
    import secrets
    secrets = secrets.__dict__
except (ImportError, SyntaxError):
    secrets = {}

SECRETS_FILE = os.path.join(os.path.dirname(__file__),'secrets.py')
SECRET_KEY = secrets.get('SECRET_KEY', 'derp')

"""
CLOUD_PROFILES contains specs and handlers of virtual machines needed for
various roles.  See the "cloud" app for details.
"""
CLOUD_PROFILES = {
    'test_ocean': {
        'provider_class': 'cloud.providers.digital_ocean.DigitalOcean',
        'oauth_token': secrets.get('DIGITAL_OCEAN_OAUTH_TOKEN', 'derp'),
        'image_id': '1216636',
        'droplet_size': '4gb',
    },
}

DATABASES = secrets.get('DATABASES', {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': 'derp',
    }
})

BASE_DIR = os.path.dirname(os.path.dirname(__file__))

DEBUG = True

TEMPLATE_DEBUG = True

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.admin',
    'django.contrib.gis',
    'enc',
    'cloud',
#    'core',
    'gunicorn',
    'south',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'hclist.urls'

WSGI_APPLICATION = 'hclist.wsgi.application'

STATIC_ROOT = os.path.join(os.path.dirname(__file__), 'static')
STATIC_URL = '/static/'

TEMPLATE_CONTEXT_PROCESSORS = (
    'django.contrib.messages.context_processors.messages',
    'django.contrib.auth.context_processors.auth',
#    'core.context_processors.add_vars',
)

